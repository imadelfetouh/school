<?php
include("logincheck.php");//include ander bestand
if($_SESSION['Levels'] == 1)
{
	
}
else
{
	//level 0 naar aanmelding.php
	header("location:aanmelding.php");
}	

require 'db.php';//database connectie

$sql 				= "SELECT * FROM aanmeldingen";// Query
$sqlBedrijfsnaam 	= "SELECT DISTINCT Bedrijfsnaam FROM `aanmeldingen`";// Query
$sqlWoonplaats 		= "SELECT DISTINCT Woonplaats FROM `aanmeldingen`";// Query
$sqlbAfstudeer		= "SELECT DISTINCT bAfstudeer FROM `aanmeldingen`";// Query
$sqlbOrienterend 	= "SELECT DISTINCT bOrienterend FROM `aanmeldingen`";// Query
$sqlsAfstudeer 		= "SELECT DISTINCT sAfstudeer FROM `aanmeldingen`";// Query
$sqlsOrienterend 	= "SELECT DISTINCT sOrienterend FROM `aanmeldingen`";// Query
$sqlaAfstudeer 		= "SELECT DISTINCT aAfstudeer FROM `aanmeldingen`";// Query
$sqlaOrienterend 	= "SELECT DISTINCT aOrienterend FROM `aanmeldingen`";// Query

$result = $conn->query($sql);//result
$resultBedrijfsnaam = $conn->query($sqlBedrijfsnaam);//result
$resultWoonplaats = $conn->query($sqlWoonplaats);//result
$resultbAfstudeer = $conn->query($sqlbAfstudeer);//result
$resultbOrienterend = $conn->query($sqlbOrienterend);//result
$resultsAfstudeer = $conn->query($sqlsAfstudeer);//result
$resultsOrienterend = $conn->query($sqlsOrienterend);//result
$resultaAfstudeer = $conn->query($sqlaAfstudeer);//result
$resultaOrienterend = $conn->query($sqlaOrienterend);//result

$bedrijfsnaam = array();//array en whiles voor filters
while($rowBedrijfsnaam = $resultBedrijfsnaam->fetch_assoc())
{
	$Bedrijfsnaam[] = $rowBedrijfsnaam;
}

$Woonplaats = array();//array en whiles voor filters
while($rowWoonplaats = $resultWoonplaats->fetch_assoc())
{
	$Woonplaats[] = $rowWoonplaats;
}

$bAfstudeer = array();//array en whiles voor filters
while($rowbAfstudeer = $resultbAfstudeer->fetch_assoc())
{
	$bAfstudeer[] = $rowbAfstudeer;
}

$bOrienterend = array();//array en whiles voor filters
while($rowbOrienterend = $resultbOrienterend->fetch_assoc())
{
	$bOrienterend[] = $rowbOrienterend;
}

$sAfstudeer = array();//array en whiles voor filters
while($rowsAfstudeer = $resultsAfstudeer->fetch_assoc())
{
	$sAfstudeer[] = $rowsAfstudeer;
}

$sOrienterend = array();//array en whiles voor filters
while($rowsOrienterend = $resultsOrienterend->fetch_assoc())
{
	$sOrienterend[] = $rowsOrienterend;
}

$aAfstudeer = array();//array en whiles voor filters
while($rowaAfstudeer = $resultaAfstudeer->fetch_assoc())
{
	$aAfstudeer[] = $rowaAfstudeer;
}

$aOrienterend = array();//array en whiles voor filters
while($rowAorienterend = $resultaOrienterend->fetch_assoc())
{
	$aOrienterend[] = $rowAorienterend;
}

//connectie sluiten
$conn->close();
?>
	
<!DOCTYPE html>
<html>
<head>
	<title></title>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/customcss.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="../js/ddtf.js"></script>
</head>
<body class="databaseBody">
	<div class="container">
	 	<table class="table" id="myTable">
	    	<thead>
	      		<tr>
			        <th>Bedrijfsnaam</th>

			        <th>Woonplaats</th>

			        <th>BeheerAF</th>

			        <th>BeheerOR</th>

			        <th>SupportAF</th>

			        <th>SupportOR</th>

			        <th>ApplicatieAF</th>

			        <th>ApplicatieOR</th>

			     	<th>Verander</th>
	     		 </tr>
	    	</thead>
	    	<?php
	    	//alle data in tabel laten zien
			if($result->num_rows > 0)		
			{
				while($row = $result->fetch_assoc())
				{
					echo "<tbody>";
						echo "<tr>";
							echo "<td>".$row['Bedrijfsnaam']."</td>";
							echo "<td>".$row['Woonplaats']."</td>";
							echo "<td>".$row['bAfstudeer']."</td>";
							echo "<td>".$row['bOrienterend']."</td>";
							echo "<td>".$row['sAfstudeer']."</td>";
							echo "<td>".$row['sOrienterend']."</td>";
							echo "<td>".$row['aAfstudeer']."</td>";
							echo "<td>".$row['aOrienterend']."</td>";
							echo "<td class='hiddenId'><input type='text' value='".$row['id']."'' name='detailId'</td>";
							echo "<td class='detailButton'><a href='detail.php?bedrijfsid=".$row['id']."'>Verander</a><td>";
						echo "</tr>";
					echo "<tbody>";
				}
			}
			?>
		</table>
	</div>
	<script>
    	$('#myTable').ddTableFilter();
    </script>
</body>
</html>