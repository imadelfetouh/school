<?php
session_start();//sessie starten
require 'db.php';//database connectie

if(isset($_POST['submit']))
{
	$Bedrijfsnaam 				= $_POST['Bedrijfsnaam'];//variabele maken
	$Woonplaats 				= $_POST['Woonplaats'];//variabele maken
	$TelefoonMobiel 			= $_POST['TelefoonMobiel'];//variabele maken
	$Contactpersoon 			= $_POST['Contactpersoon'];//variabele maken
	$Email 						= $_POST['Email'];//variabele maken
	$bAfstudeer 				= $_POST['bAfstudeer'];//variabele maken
	$bOrienterend 				= $_POST['bOrienterend'];//variabele maken
	$sAfstudeer 				= $_POST['sAfstudeer'];//variabele maken
	$sOrienterend 				= $_POST['sOrienterend'];//variabele maken
	$aAfstudeer 				= $_POST['aAfstudeer'];//variabele maken
	$aOrienterend 				= $_POST['aOrienterend'];//variabele maken
	$Opmerking 					= $_POST['Opmerking'];//variabele maken

	$query = "INSERT INTO aanmeldingen (Bedrijfsnaam, Woonplaats, TelefoonMobiel, Contactpersoon, Email, bAfstudeer, bOrienterend, sAfstudeer, sOrienterend, aAfstudeer, aOrienterend, Opmerking) VALUES ('$Bedrijfsnaam', '$Woonplaats', '$TelefoonMobiel', '$Contactpersoon', '$Email', '$bAfstudeer', '$bOrienterend', '$sAfstudeer', '$sOrienterend', '$aAfstudeer', '$aOrienterend', '$Opmerking')";//gegevens in tabel aanmeldingen zetten

	if($Bedrijfsnaam == "" || $Woonplaats == "" || $TelefoonMobiel == "" || $Contactpersoon == "" || $Email == "")
	{
		//als een input van bovenstaande velden leeg is
		echo "Vul alle velden in.";
		header("refresh:2; url=aanmelding.php");
	}
	else
	{
		if($bAfstudeer == "0" && $bOrienterend == "0" && $sAfstudeer == "0" && $sOrienterend == "0" && $aAfstudeer == "0" && $aOrienterend == "0")
		{
			//als alle van bovenstaade velden op 0 staat
			echo "Kies minimaal 1 stagiair(e).";
			header("refresh:2; url=aanmelding.php");
		}
		else
		{
			if(!mysqli_query($conn, $query))
			{
				//als de query niet is gelukt
				echo "Er is iets misgegaan. Probeer het later opnieuw!";
				header("refresh:2; url=aanmelding.php");
			}
			else
			{
				//als de query wel is gelukt
				echo "Uw gegevens zijn succesvol verzonden!";
				header("refresh:2; url=aanmelding.php");
			}
		}
	}
}
?>