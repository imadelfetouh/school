<?php
include("logincheck.php");//include ander bestand
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
 	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/customcss.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body class="aanmeldingStageBody">
	<div class="container">
		<div class="row centered-form">
			<div class="panel panel-default">
				<div class="panel-heading">
		    		<center><h3 class="panel-title">Aanmelding BPV plaats ICT</h3></center>
		 		</div>
		 		<div class="panel-body">
		    		<form role="form" action="insert.php" method="POST" id="registrationForm">
		    			<div class="row">
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		                			<input type="text" name="Bedrijfsnaam" id="Bedrijfsnaam" class="form-control input-sm" placeholder="Bedrijfsnaam" autofocus="autofocus">
		    					</div>
		    				</div>
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="Woonplaats" id="Woonplaats" class="form-control input-sm" placeholder="Woonplaats">
		    					</div>
		    				</div>
		    			</div>

		    			<div class="row">
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="TelefoonMobiel" id="Telefoon/Mobiel" class="form-control input-sm" placeholder="Telefoon/Mobiel">
		    					</div>
		    				</div>
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="Contactpersoon" id="Contactpersoon" class="form-control input-sm" placeholder="Contactpersoon">
		    					</div>
		    				</div>
		    			</div>

		    			<div class="row">
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="Email" name="Email" id="Email" class="form-control input-sm" placeholder="Email">
		    					</div>
		    				</div>
		    			</div>

		    			<div class="row">
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<label>Kies uw aantal stagiaires voor beheer afstudeer</label>
		    					<div class="form-group">
		    						<select name="bAfstudeer" class="form-control input-sm">
		    							<option>0</option>
		    							<option>1</option>
		    							<option>2</option>
		    							<option>3</option>
		    							<option>4</option>
		    							<option>5</option>
		    						</select>
		    					</div>
		    				</div>
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<label>Kies uw aantal stagiaires voor beheer orienterend</label>
		    					<div class="form-group">
		    						<select name="bOrienterend" class="form-control input-sm">
		    							<option>0</option>
		    							<option>1</option>
		    							<option>2</option>
		    							<option>3</option>
		    							<option>4</option>
		    							<option>5</option>
		    						</select>
		    					</div>
		    				</div>
		    			</div>

		    			<div class="row">
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<label>Kies uw aantal stagiaires voor support afstudeer</label>
		    						<select name="sAfstudeer" class="form-control input-sm">
		    							<option>0</option>
		    							<option>1</option>
		    							<option>2</option>
		    							<option>3</option>
		    							<option>4</option>
		    							<option>5</option>
		    						</select>
		    					</div>
		    				</div>
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<label>Kies uw aantal stagiaires voor support orienterend</label>
		    					<div class="form-group">
		    						<select name="sOrienterend" class="form-control input-sm">
		    							<option>0</option>
		    							<option>1</option>
		    							<option>2</option>
		    							<option>3</option>
		    							<option>4</option>
		    							<option>5</option>
		    						</select>
		    					</div>
		    				</div>
		    			</div>

		    			<div class="row">
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<label>Kies uw aantal stagiaires voor applicatie afstudeer</label>
		    						<select name="aAfstudeer" class="form-control input-sm">
		    							<option>0</option>
		    							<option>1</option>
		    							<option>2</option>
		    							<option>3</option>
		    							<option>4</option>
		    							<option>5</option>
		    						</select>
		    					</div>
		    				</div>
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<label>Kies uw aantal stagiaires voor applicatie orienterend</label>
		    					<div class="form-group">
		    						<select name="aOrienterend" class="form-control input-sm">
		    							<option>0</option>
		    							<option>1</option>
		    							<option>2</option>
		    							<option>3</option>
		    							<option>4</option>
		    							<option>5</option>
		    						</select>
		    					</div>
		    				</div>
		    			</div>

		    			<div class="form-group">
		    				 <textarea name="Opmerking" id="Opmerking" class="form-control input-sm" placeholder="Opmerking" style="width: 100%;"></textarea>
		    			</div>

		    			<input type="submit" value="Verzenden" class="btn btn-info btn-block" name="submit" id="submit" style="background-color: #009898;">
		    		</form>
		    	</div>
			</div>
		</div>
	</div>
</body>
</html>