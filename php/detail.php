<?php
include("logincheck.php");//include ander bestand

require('db.php');//database connectie

if(isset($_GET['bedrijfsid']))
{
	$_SESSION['id'] = $_GET['bedrijfsid'];//id
}

$detailSQL = "SELECT * FROM `aanmeldingen` where `id` = '" . $_SESSION['id'] . "'";//query

if(isset($_POST['Update']))
{
	//update query
	$sqlUpdate = "UPDATE `aanmeldingen` SET `bAfstudeer` = '".$_POST['bAfstudeer']."', `bOrienterend` = '".$_POST['bOrienterend']."', `sAfstudeer` = '".$_POST['sAfstudeer']."', `sOrienterend` = '".$_POST['sOrienterend']."', `aAfstudeer` = '".$_POST['aAfstudeer']."', `aOrienterend` = '".$_POST['aOrienterend']."', `Opmerking` = '".$_POST['Opmerking']."' WHERE `id` = '".$_SESSION['id']."' ";

	if ($conn->query($sqlUpdate) === TRUE) 
	{
		//als de query is gelukt
		echo "Succesvol veranderd";
		header("Refresh:2; url=database.php");
	}
	else 
	{
		//als de query niet is gelukt
		echo "Error updating record: " . $conn->error;
		header("Refresh:2; url=database.php");
	}
}
$detailResult = $conn->query($detailSQL);
?>
<!DOCTYPE html>
<html>
<head>
 	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/customcss.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title></title>
</head>
<body>
	<div class="container">
		<br>
        <form action="detail.php" method="POST">
			<?php
			//data laten zien in een tabel
			if($detailResult->num_rows > 0)		
			{
				while($row = $detailResult->fetch_assoc())
				{
					?>
					<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
					    <tr>
						    <th>BedrijfsNaam</th>
						    <td><input type="text" name="Bedrijfsnaam" value="<?php echo $row['Bedrijfsnaam']; ?>" style="width: 100%"></td>
					    </tr>
					    <tr>
						    <th>Woonplaats</th>
						    <td><input type="text" name="Woonplaats" value="<?php echo $row['Woonplaats']; ?>" style="width: 100%"></td>
					    </tr>
					    <tr>
						    <th>Telefoon/Mobiel</th>
						    <td><input type="text" name="TelefoonMobiel" value="<?php echo $row['TelefoonMobiel']; ?>" style="width: 100%"></td>
					    </tr>
					    <tr>
						    <th>Contactpersoon</th>
						    <td><input type="text" name="Contactpersoon" value="<?php echo $row['Contactpersoon']; ?>" style="width: 100%"></td>
					    </tr>
					    <tr>
						    <th>Email</th>
						    <td><input type="text" name="Email" value="<?php echo $row['Email']; ?>" style="width: 100%"></td>
					    </tr>
					    <tr>
						    <th>Beheer Afstudeer</th>
						    <td><input type="Number" name="bAfstudeer" value="<?php echo $row['bAfstudeer']; ?>"></td>
					    </tr>
					    <tr>
						    <th>Beheer Orienterend</th>
						    <td><input type="Number" name="bOrienterend" value="<?php echo $row['bOrienterend']; ?>"></td>
					    </tr>
					    <tr>
						    <th>Support Afstudeer</th>
						    <td><input type="Number" name="sAfstudeer" value="<?php echo $row['sAfstudeer']; ?>"></td>
					    </tr>
					    <tr>
						    <th>Support Orienterend</th>
						    <td><input type="Number" name="sOrienterend" value="<?php echo $row['sOrienterend']; ?>"></td>
					    </tr>
					    <tr>
					    	<th>Applicatie Afstudeer</th>
					    	<td><input type="Number" name="aAfstudeer" value="<?php echo $row['aAfstudeer']; ?>"></td>
					    </tr>
					    <tr>
					    	<th>Applicatie Orienterend</th>
					    	<td><input type="Number" name="aOrienterend" value="<?php echo $row['aOrienterend']; ?>"></td>
					    </tr>
					    <tr>
						    <th>Opmerking</th>
						    <td><textarea name="Opmerking" value="<?php echo $row['Opmerking']; ?>"><?php echo $row['Opmerking']; ?></textarea></td>
					    </tr>
				    </table>
					<?php
				}
			}
			else
			{
				//als er geen data is met het id in de url
				echo "Geen aanmelding gevonden met nummer ". "".$_SESSION['id']."";
				echo "<br>";
				?>
				<script>
					$(document).ready(function(){
        				$("#Update").hide();//button hiden
    				});
				</script>
				<?php
			}
			?>
			<input type='submit' name='Update' value='Opslaan' class='btn btn-primary' id='Update' />
			<a href="database.php"><button type="button" class="btn btn-default" data-dismiss="modal">Terug</button></a>
        </form>
	</div>
</body>
</html>