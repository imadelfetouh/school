<?php
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/customcss.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body class="loginpageBody">
	<div class="wrapper">
    	<form class="form-signin" action="php/login.php" method="POST">       
    		<center><h2 class="form-signin-heading">Login</h2></center>
    		<label>Login naam</label>
    		<input type="text" class="form-control" name="username" placeholder="Login naam" required="" autofocus="" />
    		<br>
    		<label>Wachtwoord</label>
    		<input type="password" class="form-control" name="password" placeholder="Wachtwoord" required=""/>     
    		<br> 
    		<input type="submit" name="myLogin" class="btn btn-lg btn-primary btn-block" value="Login"> 
            <br>
            <center>Ontwikkeld door Imad El Fetouh</center>
    	</form>
  </div>
</body>
</html>